
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


function getCarModelAlphabetically(inventory){
    if(Array.isArray(inventory)){
        let carModelAphetically = [];
        carModelAphetically = inventory.map(car => car.car_model);
        return carModelAphetically.sort();
    }
    else{
        return [];
    }
}

module.exports = getCarModelAlphabetically;