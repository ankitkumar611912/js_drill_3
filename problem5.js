// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function getAllCarsOlderThan2000(getAllYearsDetails){
    if(Array.isArray(getAllYearsDetails)){
        const allCarsOlderThan2000 = getAllYearsDetails.filter(year => year < 2000 )
        return allCarsOlderThan2000;
    }
    else{
        return [];
    }
}

module.exports = getAllCarsOlderThan2000;